"""
小町算の結果一覧を取得する.
計算にはそれなりに時間がかかる.
"""

from itertools import product
from collections import defaultdict
from sympy import expand
import json

d = defaultdict(list)

for cnt, perm in enumerate(product(['+', '-', '*', '/', ''], repeat=8)):
    perm = list(perm) + ['']
    expr = ''.join(f'{i}{perm[i-1]}' for i in range(1, 9 + 1))
    d[str(expand(expr))].append(expr)

    if cnt % 100 == 0 and cnt > 0:
        print(cnt)

with open('komachi.json', 'w') as f:
    json.dump(dict(d), f, indent=4)

